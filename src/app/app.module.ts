import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpModule} from '@angular/http';
import { appRouting } from './app.routes';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ListajugadoresComponent } from './components/listajugadores/listajugadores.component';
import { DisponiblesComponent } from './components/disponibles/disponibles.component';
import { AltajugadoresComponent } from './components/altajugadores/altajugadores.component';
import { FifaService } from './components/servicios/fifa.service';
import { NavbarComponent } from './components/navbar/navbar.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DisponiblesComponent,
    AltajugadoresComponent,
    NavbarComponent,
    ListajugadoresComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule
  ],
  providers: [FifaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
