//   Archivo de rutas

import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component'
import { ListajugadoresComponent } from './components/listajugadores/listajugadores.component'
import { DisponiblesComponent } from './components/disponibles/disponibles.component'
import { AltajugadoresComponent } from './components/altajugadores/altajugadores.component'




const routes: Routes = [
    { path: 'home', component: HomeComponent },   
    { path: 'listajugadores', component: ListajugadoresComponent },
    { path: 'altajugador', component: AltajugadoresComponent },   
    { path: 'disponibles', component: DisponiblesComponent },   
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];

export const appRouting = RouterModule.forRoot(routes);