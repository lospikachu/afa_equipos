import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltajugadoresComponent } from './altajugadores.component';

describe('AltajugadoresComponent', () => {
  let component: AltajugadoresComponent;
  let fixture: ComponentFixture<AltajugadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltajugadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltajugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
